import express from 'express'
import UserModel from '../models/UserModel'

const router = express.Router()

// Routes HTTP GET - Get all users
router.route('/').get( (req, res) => {
    UserModel.find((err, results) => {
        res.send(results)
    })
  })

// Routes HTTP Post - Create User
router.route('/').post( (req, res) => {
    let user = new UserModel()

    user.email = req.body.email
    user.firstName = req.body.firstName
    user.lastName = req.body.lastName
    user.password = req.body.password

    user.save((err, newUser) => {
        if (err) res.send(err)
        else res.send(newUser)
    })
})
  
// Routes HTTP Put - Update User
router.route('/:id').put( (req, res) => {
    const id = req.params.id
    // Search User with id
    UserModel.findById(id,  (err, user) => {
        if(!err) {
            if(user) {
                const {email, firstName, lastName, password} = req.body;
                // edit only defined fields
                if(email)
                    user.email = email
                if(firstName)
                    user.firstName = firstName
                if(lastName)
                    user.lastName = lastName
                if(password)
                    user.password = password

                // Method save new values of fields
                user.save((err) => {
                    if(!err){
                        user.password = undefined //Delete password data field for security when resend user modified
                        res.status(200).json(user) //Saved
                    } else {
                        res.status(304).json(error) //Not modified
                    }
                })
            } else {
                res.status(400).json({message:'User not founded.'})//Not founded
            }
        } else {
            res.status(400).json(err)// Not a valid format Mongo Id or general error
        }
    })
})
  
// Routes HTTP Put - Delete User
router.route('/:id').delete( (req, res) => {
    const id = req.params.id
    // Method find User and remove
    UserModel.findByIdAndRemove(id, (err, user) => {
        if(!err) {
            if(user != null)
                res.status(200).json({message: 'User ' + id + ' deleted.' })
            else
                res.status(400).json({message:'User not founded.'})
        } else {
            res.status(400).json(err)
        }
    })
})

module.exports = router;