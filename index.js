import { connect } from 'mongoose'
import bodyParser from 'body-parser'
import express from 'express'
import cors from 'cors' //Cross origin resource sharing

import usersRouter from './routes/users-route';

// Method - Connect to database MongoDB
connect('mongodb://test:yakkyo@ds135069.mlab.com:35069/test-yakkyo', 
  { useNewUrlParser: true, 
    useCreateIndex: true, 
    useFindAndModify: false  })

const app = express() //Create Istance saved on app
app.use(cors()) // Enables interact with other resources (different origin)
app.use(bodyParser.urlencoded({ extended: true })) //to handle HTTP post request to req.body
app.use(bodyParser.json()) //use lightweight data-interchange format json

// Routes HTTP GET - Test
app.get('/', (req, res) => {
  res.send('Just a test')
})

app.use('/users', usersRouter);

// Set listening port
app.listen(8080, () => console.log('Example app listening on port 8080!'))
